package model;

import com.google.gson.Gson;
import java.util.ArrayList;

/**
 *
 * @author Marcio
 */
public class Loja {
    
    private int id;
    private String loja;
    private String senha;
    private String endereco;
    private String cnpj;
    private ArrayList<Vendedor> vendedores = new ArrayList<>();
    private ArrayList<Produto> produtos = new ArrayList<>();
    private ArrayList<Integer> quantidade = new ArrayList<Integer>();

    public Loja(String loja, String senha, String endereco, String cnpj) {
        this.loja = loja;
        this.senha = senha;
        this.endereco = endereco;
        this.cnpj = cnpj;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLoja() {
        return loja;
    }

    public void setLoja(String loja) {
        this.loja = loja;
    }

    
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public ArrayList<Vendedor> getVendedores() {
        return vendedores;
    }

    public void setVendedores(ArrayList<Vendedor> vendedores) {
        this.vendedores = vendedores;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public ArrayList<Integer> getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(ArrayList<Integer> quantidade) {
        this.quantidade = quantidade;
    }
    
    public void addVendedor(Vendedor vendedor){
        vendedores.add(vendedor);
    }
    
    public void removeVendedor(Vendedor vendedor){
        for (int i = 0; i < vendedores.size(); i++) {
            Vendedor v = vendedores.get(i);
            if (v.equals(vendedor)) {
                vendedores.remove(vendedor);
            }
        }
    }
    
    public void addProduto(Produto p, int quantidade){
        produtos.add(p);
        this.quantidade.add(quantidade);
    }
    
    public void removeProduto(Produto produto){
        for (int i = 0; i < produtos.size(); i++) {
            Produto p = produtos.get(i);
            if(p.equals(produto)){
                produtos.remove(i);
                quantidade.remove(i);
            }
        }
    }
    
    public String gerarJson(){
        Gson gson = new Gson();
        String aux = gson.toJson(this);
        return aux;
    }
}
