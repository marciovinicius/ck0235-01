package model;

import com.google.gson.Gson;
import java.util.Date;

/**
 *
 * @author Marcio
 */
public class Vendedor extends Pessoa{
    
    private int id;
    private Date dataCadastro;
    private double salario;

    public Vendedor(Date dataCadastro, double salario, int cpf, 
            String nome, String endereco, Date dataNascimento, String sexo) {
        super(cpf, nome, endereco, dataNascimento, sexo);
        this.dataCadastro = dataCadastro;
        this.salario = salario;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String gerarJson(){
        Gson gson = new Gson();
        String aux = gson.toJson(this);
        return aux;
    }    
}
