package model;

import com.google.gson.Gson;

/**
 *
 * @author Marcio
 */
public class Entrega {
    
    private int id;
    private Transportadora transportadora;
    private Pedido pedido;

    public Entrega(Transportadora transportadora, Pedido pedido) {
        this.transportadora = transportadora;
        this.pedido = pedido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Transportadora getTransportadora() {
        return transportadora;
    }

    public void setTransportadora(Transportadora transportadora) {
        this.transportadora = transportadora;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }
    
    public String gerarJson(){
        Gson gson = new Gson();
        String aux = gson.toJson(this);
        return aux;
    }
}
