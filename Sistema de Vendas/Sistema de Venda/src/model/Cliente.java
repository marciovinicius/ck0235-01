package model;

import com.google.gson.Gson;
import java.util.Date;

/**
 *
 * @author Marcio
 */
public class Cliente extends Pessoa {
    
    private int idCliente;
    private Date dataCadastro;

    public Cliente(Date dataCadastro, int cpf, String nome, 
            String endereco, Date dataNascimento, String sexo) {
        super(cpf, nome, endereco, dataNascimento, sexo);
        this.dataCadastro = dataCadastro;
    }   

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;        
    }
    
    public String gerarJson(){
        Gson gson = new Gson();
        String aux = gson.toJson(this);
        return aux;
    }
}
