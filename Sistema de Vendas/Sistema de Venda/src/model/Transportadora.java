package model;

import com.google.gson.Gson;

/**
 *
 * @author Marcio
 */
public class Transportadora {
    
    private int id;
    private String nome;
    private double taxa;

    public Transportadora(String nome, double taxa) {
        this.nome = nome;
        this.taxa = taxa;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getTaxa() {
        return taxa;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }
    
    public String gerarJson(){
        Gson gson = new Gson();
        String aux = gson.toJson(this);
        return aux;
    }   
}
