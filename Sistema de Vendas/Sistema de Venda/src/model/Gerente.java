package model;

import com.google.gson.Gson;
import java.util.Date;

/**
 *
 * @author Marcio
 */
public class Gerente extends Pessoa {
    
    private int id;
    private String senha;

    public Gerente(String senha, int cpf, String nome, String endereco,
            Date dataNascimento, String sexo) {
        super(cpf, nome, endereco, dataNascimento, sexo);
        this.senha = senha;
    }    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public String gerarJson(){
        Gson gson = new Gson();
        String aux = gson.toJson(this);
        return aux;
    }
}
