package model;

import com.google.gson.Gson;
import java.util.ArrayList;

/**
 *
 * @author Marcio
 */
public class Pedido {
    
    private int id;
    private Cliente cliente;
    private Vendedor vendedor;
    private double precoTotal;
    private ArrayList<Produto> pedido = new ArrayList<>();
    private ArrayList<Integer> quantidade = new ArrayList<>();

    public Pedido(Cliente cliente, Vendedor vendedor) {
        this.cliente = cliente;
        this.vendedor = vendedor;
        this.precoTotal = 0.0;
    }

    public int getId() {
        return id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public ArrayList<Produto> getPedido() {
        return pedido;
    }

    public void setPedido(ArrayList<Produto> pedido) {
        this.pedido = pedido;
    }

    public ArrayList<Integer> getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(ArrayList<Integer> quantidade) {
        this.quantidade = quantidade;
    }

    public double getPrecoTotal() {
        return precoTotal;
    }
    
    public void addProduto(Produto p, int quantidade){
        pedido.add(p);
        this.quantidade.add(quantidade);
        precoTotal += p.getPreco() * quantidade;
    }
    
    public void removeProduto(Produto produto){
        for (int i = 0; i < pedido.size(); i++) {
            Produto p = pedido.get(i);
            if(p.equals(produto)){
                precoTotal -= produto.getPreco() * quantidade.get(i);
                pedido.remove(i);
                quantidade.remove(i);
            }
        }
    }
    
    public String gerarJson(){
        Gson gson = new Gson();
        String aux = gson.toJson(this);
        return aux;
    }
}
