package model;

import java.util.Date;

/**
 *
 * @author Marcio
 */
public abstract class Pessoa {
    protected int cpf;
    protected String nome;
    protected String endereco;
    protected Date dataNascimento;
    protected String sexo;   

    public Pessoa(int cpf, String nome, String endereco, Date dataNascimento, String sexo) {
        this.cpf = cpf;
        this.nome = nome;
        this.endereco = endereco;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getCpf() {
        return cpf;
    }

    public void setCpf(int cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }    

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    
}
