package controller;

import banco.Lojas;
import java.util.ArrayList;
import model.Loja;
import banco.Gerenciador;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author msarmento
 */
public class LojaController {
    
    private static Gerenciador gerenciador = new Gerenciador();
    
    public boolean acesso(String usuario, String senha){
        
        Lojas loja;
        boolean ok = false;
        try {
            loja = gerenciador.criarLojas();
            ArrayList<Loja> lojas = loja.getLojas();
            for(int i=0; i<lojas.size(); i++){
                if(lojas.get(i).getLoja().equals(usuario) && 
                        lojas.get(i).getSenha().equals(senha)){
                    ok = true;
                    break;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LojaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ok;
    }
    
    public boolean criarConta(String nomeLoja, String senha, String endereco, String cnpj){
        boolean ok = false;
        Loja loja = new Loja(nomeLoja, senha, endereco, cnpj);

        Lojas lojas;
        try {
            lojas = gerenciador.criarLojas();
            lojas.addLoja(loja);
            ok = true;
        } catch (IOException ex) {
            Logger.getLogger(LojaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ok;
    }
}
