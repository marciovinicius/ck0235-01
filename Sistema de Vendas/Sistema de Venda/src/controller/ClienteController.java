package controller;

import banco.Clientes;
import banco.Gerenciador;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
/**
 *
 * @author msarmento
 */
public class ClienteController {
    
    private static Gerenciador gerenciador = new Gerenciador();
    
    public boolean cadastro(String nome, int cpf, String endereco, String sexo, Date dataNascimento){
        boolean ok = false;
        Date dataCadastro = new Date();
        Cliente cliente = new Cliente(dataCadastro, cpf, nome, endereco, dataNascimento, sexo);
        Clientes clientes;
        try {
            clientes = gerenciador.criarClientes();
            clientes.addCliente(cliente);
            ok = true;
        } catch (IOException ex) {
            Logger.getLogger(nome).log(Level.SEVERE, null, ex);
        }
       
        return ok;
    }
}
