package controller;

import banco.Gerenciador;
import banco.Transportadoras;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Transportadora;

/**
 *
 * @author msarmento
 */
public class TransportadoraController {
    
    private Gerenciador gerenciador = new Gerenciador();
    
    public boolean cadastro(String nome, Double taxa){
        boolean ok = false;
        Transportadora transportadora = new Transportadora(nome, taxa);
        
        try {
            Transportadoras transportadoras = gerenciador.criarTransportadoras();
            transportadoras.addTransportadora(transportadora);
            ok = true;
        } catch (IOException ex) {
            Logger.getLogger(TransportadoraController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ok;
    }
}
