package controller;

import banco.Gerenciador;
import banco.Vendedores;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Vendedor;

/**
 *
 * @author msarmento
 */
public class VendedorController {
    
    private Gerenciador gerenciador = new Gerenciador();
    
    public boolean cadastro(String nome, String endereco, Date dataNascimento, String sexo, double salario, int cpf){
        boolean ok = false;
        Date dataCadastro = new Date();
        Vendedor vendedor = new Vendedor(dataCadastro, salario, cpf, nome, endereco, dataNascimento, sexo);
        
        try {
            Vendedores vendedores = gerenciador.criarVendedores();
            vendedores.addVendedores(vendedor);
            ok = true;
        } catch (IOException ex) {
            Logger.getLogger(VendedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ok;
    }
}  
