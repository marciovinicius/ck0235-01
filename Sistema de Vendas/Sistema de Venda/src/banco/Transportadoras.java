/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Transportadora;

/**
 *
 * @author msarmento
 */
public class Transportadoras {
    private ArrayList<Transportadora> transportadoras = new ArrayList<>();
    
    public ArrayList<Transportadora> getTransportadoras() {
        return transportadoras;
    }

    public void setTransportadoras(ArrayList<Transportadora> transportadoras) {
        this.transportadoras = transportadoras;
    }
    
    public void addTransportadora(Transportadora transportadora){
        transportadora.setId(transportadoras.size()+1);
        transportadoras.add(transportadora);
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Transportadoras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeTransportadora(Transportadora transportadora){
        for(int i=0; i<transportadoras.size(); i++){
            if(transportadoras.get(i).equals(transportadora)){
                transportadoras.remove(i);
            }
        }
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Transportadoras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizarArquivo() throws IOException {
        File file = new File(".\\src\\arquivos\\transportadoras.txt");
        try (FileWriter fw = new FileWriter(file)) {
            Gson json = new Gson();
            String str = json.toJson(this);
            fw.write(str);
        }
    }
}
