/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Entrega;

/**
 *
 * @author msarmento
 */
class Entregas {
    private ArrayList<Entrega> entregas = new ArrayList<>();
    
    public ArrayList<Entrega> getEntregas() {
        return entregas;
    }

    public void setEntregas(ArrayList<Entrega> entregas) {
        this.entregas = entregas;
    }
    
    public void addEntrega(Entrega Entrega){
        Entrega.setId(entregas.size()+1);
        entregas.add(Entrega);
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Entregas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeEntrega(Entrega Entrega){
        for(int i=0; i<entregas.size(); i++){
            if(entregas.get(i).equals(Entrega)){
                entregas.remove(i);
            }
        }
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Entregas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizarArquivo() throws IOException {
        File file = new File(".\\src\\arquivos\\entregas.txt");
        try (FileWriter fw = new FileWriter(file)) {
            Gson json = new Gson();
            String str = json.toJson(this);
            fw.write(str);
        }
    }
}
