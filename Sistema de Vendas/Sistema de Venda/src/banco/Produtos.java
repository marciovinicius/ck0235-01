/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Produto;

/**
 *
 * @author msarmento
 */
public class Produtos {
    private ArrayList<Produto> produtos = new ArrayList<>();
    
    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public void addProduto(Produto produto){
        produto.setId(produtos.size()+1);
        produtos.add(produto);
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Produtos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeProduto(Produto produto){
        for(int i=0; i<produtos.size(); i++){
            if(produtos.get(i).equals(produto)){
                produtos.remove(i);
            }
        }
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Produtos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizarArquivo() throws IOException {
        File file = new File(".\\src\\arquivos\\produtos.txt");
        try (FileWriter fw = new FileWriter(file)) {
            Gson json = new Gson();
            String str = json.toJson(this);
            fw.write(str);
        }
    }
}
