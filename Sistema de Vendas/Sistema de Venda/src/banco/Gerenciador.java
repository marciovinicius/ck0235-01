/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;


import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author msarmento
 */
public class Gerenciador {
    
    public Clientes criarClientes() throws FileNotFoundException, IOException{
        File file = new File(".\\src\\arquivos\\clientes.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String classe = br.readLine();
        Gson json = new Gson();
        Clientes clientes = new Clientes();
        clientes = json.fromJson(classe, Clientes.class);
        return clientes;
    }
    
    public Lojas criarLojas() throws FileNotFoundException, IOException{
        File file = new File(".\\src\\arquivos\\lojas.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String classe = br.readLine();
        Gson json = new Gson();
        Lojas lojas = new Lojas();
        lojas = json.fromJson(classe, Lojas.class);
        return lojas;
    }
    
    public Pedidos criarPedidos() throws FileNotFoundException, IOException{
        File file = new File(".\\src\\arquivos\\pedidos.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String classe = br.readLine();
        Gson json = new Gson();
        Pedidos pedidos = new Pedidos();
        pedidos = json.fromJson(classe, Pedidos.class);
        return pedidos;
    }
    
    public Produtos criarProdutos() throws FileNotFoundException, IOException{
        File file = new File(".\\src\\arquivos\\produtos.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String classe = br.readLine();
        Gson json = new Gson();
        Produtos produtos = new Produtos();
        produtos = json.fromJson(classe, Produtos.class);
        return produtos;
    }
    
    public Transportadoras criarTransportadoras() throws FileNotFoundException, IOException{
        File file = new File(".\\src\\arquivos\\transportadoras.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String classe = br.readLine();
        Gson json = new Gson();
        Transportadoras transportadoras = new Transportadoras();
        transportadoras = json.fromJson(classe, Transportadoras.class);
        return transportadoras;
    }
    
    public Vendedores criarVendedores() throws FileNotFoundException, IOException{
        File file = new File(".\\src\\arquivos\\vendedores.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String classe = br.readLine();
        Gson json = new Gson();
        Vendedores vendedores = new Vendedores();
        vendedores = json.fromJson(classe, Vendedores.class);
        return vendedores;
    }
    
    public Entregas criarEntregas() throws FileNotFoundException, IOException{
        File file = new File(".\\src\\arquivos\\entregas.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String classe = br.readLine();
        Gson json = new Gson();
        Entregas entregas = new Entregas();
        entregas = json.fromJson(classe, Entregas.class);
        return entregas;
    }
}
