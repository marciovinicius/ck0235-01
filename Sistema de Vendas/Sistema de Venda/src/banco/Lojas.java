/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Loja;

/**
 *
 * @author msarmento
 */
public class Lojas {
    private ArrayList<Loja> lojas = new ArrayList<>();
    
    public ArrayList<Loja> getLojas() {
        return lojas;
    }

    public void setLojas(ArrayList<Loja> lojas) {
        this.lojas = lojas;
    }
    
    public void addLoja(Loja loja){
        loja.setId(lojas.size()+1);
        lojas.add(loja);
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Loja.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeLoja(Loja loja){
        for(int i=0; i<lojas.size(); i++){
            if(lojas.get(i).equals(loja)){
                lojas.remove(i);
            }
        }
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Lojas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizarArquivo() throws IOException {
        File file = new File(".\\src\\arquivos\\lojas.txt");
        try (FileWriter fw = new FileWriter(file)) {
            Gson json = new Gson();
            String str = json.toJson(this);
            fw.write(str);
        }
    }
}
