/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Vendedor;

/**
 *
 * @author msarmento
 */
public class Vendedores {
    private ArrayList<Vendedor> vendedores = new ArrayList<>();
    
    public ArrayList<Vendedor> getVendedores() {
        return vendedores;
    }

    public void setVendedores(ArrayList<Vendedor> vendedores) {
        this.vendedores = vendedores;
    }
    
    public void addVendedores(Vendedor vendedor){
        vendedor.setId(vendedores.size()+1);
        vendedores.add(vendedor);
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Vendedores.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeVendedor(Vendedor vendedor){
        for(int i=0; i<vendedores.size(); i++){
            if(vendedores.get(i).equals(vendedor)){
                vendedores.remove(i);
            }
        }
        try {
            atualizarArquivo();
        } catch (IOException ex) {
            Logger.getLogger(Vendedores.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizarArquivo() throws IOException {
        File file = new File(".\\src\\arquivos\\vendedores.txt");
        try (FileWriter fw = new FileWriter(file)) {
            Gson json = new Gson();
            String str = json.toJson(this);
            fw.write(str);
        }
    }
}
